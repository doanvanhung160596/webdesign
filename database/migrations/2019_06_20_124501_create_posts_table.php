<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
			$table->string('description');
			$table->string('slug')->unique();
            $table->boolean('active');
            $table->string('image')->nullable();
            $table->string('header_description')->nullable();
			$table->string('header_background')->nullable();
			$table->string('skills')->nullable()->comment('Kỹ năng công nghệ làm dự án');
			$table->string('type')->nullable()->comment('Loại công nghệ làm dự án');
			$table->string('link_youtube')->nullable()->comment('Link Youtube for Talk');
            $table->text('body');
            $table->integer('user_id');
            $table->integer('category_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
