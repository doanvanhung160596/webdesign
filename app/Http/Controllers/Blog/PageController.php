<?php

namespace App\Http\Controllers\Blog;

use App\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    public function index($slug){
    	$page = Page::where('slug', $slug)->firstOrFail();
    	if($page->slug === 'cv'){
			return view('blog.page.cv', compact('page'));
		}
    	return view('blog.page.index', compact('page'));
	}
}
