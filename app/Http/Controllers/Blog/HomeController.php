<?php

namespace App\Http\Controllers\Blog;

use App\CategoryPost;
use App\Helpers\Constants;
use App\Post;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
	/**
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function index(){
//		Project
    	$category_project = CategoryPost::where('slug', Constants::PROJECTS)->firstOrFail();
    	$projects = Post::where('category_id', $category_project->id)->take(3)->get();
		if (count($projects) > 0){
			$result = [];
			foreach ($projects as $project){
				$project->skills = explode(',', $project->skills);
				$result[] = $project;
			}
			$projects = $result;
		}
//		Post
		$category_post = CategoryPost::where('slug', Constants::BLOG)->firstOrFail();
		$posts = Post::where('category_id', $category_post->id)->take(3)->get();
    	return view('blog.home.index', compact('category_post', 'category_project', 'projects', 'posts'));
	}
}
