<?php

namespace App\Http\Controllers\Blog;

use App\CategoryPost;
use App\Post;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($category)
    {
		$category = CategoryPost::where('slug',$category)->firstOrFail();
		$posts = Post::where('category_id', $category->id)->get();
		if ($category->slug === 'projects'){
			if (count($posts) > 0){
				$result = [];
				foreach ($posts as $post){
					$post->skills = explode(',', $post->skills);
					$result[] = $post;
				}
				$posts = $result;
			}
			return view('blog.post.project.index', compact('posts', 'category'));
		}elseif ($category->slug === 'talks'){
			return view('blog.post.talk.index', compact('posts', 'category'));
		}
		return view('blog.post.blog.index', compact('posts', 'category'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($category, $slug)
    {
		$category = CategoryPost::where('slug',$category)->firstOrFail();
		$post = Post::where('slug', $slug)->firstOrFail();
		$posts_related = Post::where('category_id', $category->id)->where('slug','!=', Request::segment(3))->take(3)->get();
		$posts_project_related = Post::where('category_id', $category->id)->where('slug','!=', Request::segment(3))->take(3)->get();
		if (count($posts_project_related) > 0){
			$result = [];
			foreach ($posts_project_related as $post){
				$post->skills = explode(',', $post->skills);
				$result[] = $post;
			}
			$posts_project_related = $result;
		}
		if ($category->slug === 'projects'){
			return view('blog.post.project.detail', compact('post', 'category', 'posts_project_related'));
		}elseif ($category->slug === 'talks'){
			return view('blog.post.talk.detail', compact('post', 'category', 'posts_related'));
		}
		return view('blog.post.blog.detail', compact('post', 'category', 'posts_related'));
    }
}
