<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	protected $guarded = ['id'];

    public function category(){
        return $this->belongsTo(CategoryPost::class, 'category_id', 'id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function getCreatedAtAttribute($value)
	{
		return Carbon::create($value)->diffForHumans();
	}
}
