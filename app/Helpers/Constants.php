<?php

namespace App\Helpers;

class Constants
{
	/* Tên Menu */
	const PAGE 	 	 = 'page';
	const POST	 	 = 'post';
	const PROJECTS	 = 'projects';
	const CONTACT	 = 'contact';
	const BLOG	 	 = 'blog';
}