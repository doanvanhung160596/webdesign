<?php

namespace App\Nova;

use App\CategoryPost;
use App\Nova\Metrics\DeactivePosts;
use App\Nova\Metrics\NewPosts;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Gravatar;
use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\Image;
use Laravel\Nova\Fields\Markdown;
use Laravel\Nova\Fields\Password;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Http\Requests\NovaRequest;
use Waynestate\Nova\CKEditor;

class Post extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = 'App\Post';

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'title', 'description'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            BelongsTo::make('User','user','App\Nova\User')
				->withMeta([
					'belongsToId' => 1
				])
                ->rules('required'),

            BelongsTo::make('Category','category','App\Nova\CategoryPost')
                ->rules('required'),

            Text::make('Title')
                ->sortable()
                ->rules('required', 'max:255'),

			Text::make('Slug')
				->sortable()
				->rules('required', 'max:255'),

			Text::make('Description')
                ->sortable()
                ->rules('required', 'max:255')
				->hideFromIndex(),

			Text::make('Header Description', 'header_description')
				->sortable()
				->hideFromIndex(),

			Text::make('Skills Of Project', 'skills')
				->sortable()
				->hideFromIndex(),

			Text::make('Type Of Project', 'type')
				->sortable()
				->hideFromIndex(),

			Text::make('Link Youtube Of Talk', 'link_youtube')
				->hideFromIndex(),

            Image::make('Image'),

			Image::make('Header Background', 'header_background'),

			CKEditor::make('Body')
                ->rules('required')
				->hideFromIndex(),

            Boolean::make('Active')
                ->trueValue(true)
                ->falseValue(false)
				->withMeta(["value" => true]),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [
            (new NewPosts)->width('1/2'),
            (new DeactivePosts)->width('1/2')
        ];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }

    public function category(){
        return $this->belongsTo(CategoryPost::class, 'category_id', 'id');
    }
}
