<?php

namespace App\Providers;

use App\Helpers\Constants;
use App\Menu;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('blog.partials.header', function($view){
			$menus = Menu::get();
			$page = Constants::PAGE;
			$post = Constants::POST;
			/*ACTIVE*/
			$type = Request::segment(1) ? Request::segment(1) : NULL;
			$slug_type = Request::segment(2) ? Request::segment(2) : NULL;
        	$view->with(compact('menus', 'page', 'post', 'type', 'slug_type'));
		});
    }
}
