@extends('blog.layout_404')
@section('title', __('Not Found'))
@section('content')
        <div style="height: 100vh; width: 100vw; background-size: cover; background-image: url({{ asset('blog/index/404.png ') }}) !important;">
            <div class="container-fluid" style="padding-top:200px;">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h1 id="jobtitle" itemprop="jobtitle">{{ trans('home.404') }}</h1>
                            <p>{{ trans('home.call_back') }} <a href="{{ route('home') }}"> {{ trans('home.home') }}</a> </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
