<div style="background-color: #536976;">
    <section class="section-cta py-4 text-white">
        <div class="container">
            <div class="text-center">
                <img class="profile-small d-inline-block mx-auto rounded-circle mb-2" src="{{ asset('blog/index/profile.jpg') }}" alt="">
            </div>
            <h3 class="section-title font-weight-bold text-center mb-2">{{ trans('home.client_project') }}</h3>
            <div class="section-intro mx-auto text-center mb-3">
                {{ trans('home.contact_one') }} <a href="{{ route('page', 'contact') }}" style="color: #ace6fd">{{ trans('home.contact_two') }}</a>
            </div>
        </div><!--//container-->
    </section>
    <footer class=" text-light text-center py-2">
        <small class="copyright">{{ trans('home.development') }}</small>
    </footer>
</div>