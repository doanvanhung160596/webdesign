@extends('blog.layout')
@section('title')
    {{ $page->title }}
@endsection
@section('css')
    style="min-height: 50vh !important; 
    background-size: cover; 
     background-image: url('{{ asset("storage/$page->header_background")}}') !important;"
@endsection
@section('header')
<div style="opacity: 1;transform: translateY(0px) translateZ(0px); margin: 50px 0px;">
    <div class="header-intro text-white py-5">
        <div class="container text-center">
            <h2 class="page-heading mb-0">{{ $page->title }}</h2>
        </div><!--//container-->
    </div>
</div>
@endsection
@section('content')
    <section class="section py-5">
        <div class="container">
            {!! $page->body !!}
        </div><!--//container-->
    </section>
@endsection
