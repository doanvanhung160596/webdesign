@extends('blog.layout')
@section('title', trans('home.title_home'))
@section('css')
    style="min-height: 100vh !important; background-size: cover; background-image: url('https://images.wallpaperscraft.com/image/sky_birds_flight_135782_1600x1200.jpg') !important;"
@endsection
@section('header')
    <style>
        .btn-contact{
            border-radius: 50px;
            background: white;
            color: #4c4c4c;
            padding: 10px 50px;
            font-size: 16px;
        }
        .btn-contact:hover{
            background: #4c4c4cb3;
            color: white;
            /*text-decoration: underline;*/
        }
    </style>
<div style="opacity: 1;transform: translateY(0px) translateZ(0px); margin: 100px 0px;">
    <div class="header-intro text-white py-5">
        <div class="container">
            <div class="profile-teaser media flex-column flex-md-row">
                <img class="profile-image mb-3 mb-md-0 mr-md-5 ml-md-0 rounded mx-auto" style="border-radius: 50% !important;" src="{{ asset('blog/index/profile.jpg')}}" alt="">
                <div class="media-body text-center text-md-left">
                    <div class="lead">{{ trans('home.hello') }}</div>
                    <h2 class="mt-0 display-4 font-weight-bold">{{ trans('home.my_name') }}</h2>
                    <div class="bio mb-3">{{ trans('home.code') }}
                        <br>
                        {{ trans('home.contact_tome')}}
                    </div><!--//bio-->
                    <a class="btn theme-btn-cta btn-contact" href="{{ route('page','contact') }}">{{ trans('home.contact_to_me') }}</a>
                </div><!--//media-body-->
            </div><!--//profile-teaser-->
        </div><!--//container-->
    </div><!--//header-intro-->
</div>
@endsection
@section('content')
    <section class="skills-section section py-5">
        <div class="container">
            <h3 class="section-title font-weight-bold text-center mb-3">{{ trans('home.total_skills') }}</h3>
            <div class="section-intro mx-auto text-center mb-5 text-secondary">
                {{ trans('home.content_skill') }}
                <br>
                <a style="color: #2a9798;" href="{{ route('page','cv') }}">
                    {{ trans('home.see_cv') }}
                </a>.
            </div>

            <div class="skills-blocks mx-auto pt-5">
                <div class="row">
                    <div class="skills-block col-12 col-lg-4 mb-5 mb-3 mb-lg-0">
                        <div class="skills-block-inner bg-white shadow-sm py-4 px-5 position-relative">
                            <h4 class="skills-cat text-center mb-3 mt-5">Frontend</h4>
                            <div class="skills-icon-holder position-absolute d-inline-block rounded-circle text-center">
                                <img class="skills-icon" src="{{ asset('blog/index/frontend-icon.svg') }}">
                            </div>
                            <ul class="skills-list list-unstyled text-secondary">
                                <li class="mb-2"><svg class="svg-inline--fa fa-check fa-w-16 mr-2 text-primary" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"></path></svg><!-- <i class="fas fa-check mr-2 text-primary"></i> -->Javascript</li>
                                <li class="mb-2"><svg class="svg-inline--fa fa-check fa-w-16 mr-2 text-primary" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"></path></svg><!-- <i class="fas fa-check mr-2 text-primary"></i> -->VueJs</li>
                                <li class="mb-2"><svg class="svg-inline--fa fa-check fa-w-16 mr-2 text-primary" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"></path></svg><!-- <i class="fas fa-check mr-2 text-primary"></i> -->Jquery</li>
                                <li class="mb-2"><svg class="svg-inline--fa fa-check fa-w-16 mr-2 text-primary" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"></path></svg><!-- <i class="fas fa-check mr-2 text-primary"></i> -->HTML</li>
                                <li class="mb-2"><svg class="svg-inline--fa fa-check fa-w-16 mr-2 text-primary" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"></path></svg><!-- <i class="fas fa-check mr-2 text-primary"></i> -->CSS</li>
                            </ul>
                        </div><!--//skills-block-inner-->
                    </div><!--//skills-block-->

                    <div class="skills-block col-12 col-lg-4 mb-5 mb-3 mb-lg-0">
                        <div class="skills-block-inner bg-white shadow-sm py-4 px-5 position-relative">
                            <h4 class="skills-cat text-center mb-3 mt-5">Backend</h4>
                            <div class="skills-icon-holder position-absolute d-inline-block rounded-circle text-center">
                                <img class="skills-icon" src="{{ asset('blog/index/backend-icon.svg') }}">
                            </div>
                            <ul class="skills-list list-unstyled text-secondary">
                                <li class="mb-2"><svg class="svg-inline--fa fa-check fa-w-16 mr-2 text-primary" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"></path></svg><!-- <i class="fas fa-check mr-2 text-primary"></i> -->PHP</li>
                                <li class="mb-2"><svg class="svg-inline--fa fa-check fa-w-16 mr-2 text-primary" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"></path></svg><!-- <i class="fas fa-check mr-2 text-primary"></i> -->OpenCart</li>
                                <li class="mb-2"><svg class="svg-inline--fa fa-check fa-w-16 mr-2 text-primary" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"></path></svg><!-- <i class="fas fa-check mr-2 text-primary"></i> -->Laravel/CodeIgniter</li>
                                <li class="mb-2"><svg class="svg-inline--fa fa-check fa-w-16 mr-2 text-primary" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"></path></svg><!-- <i class="fas fa-check mr-2 text-primary"></i> -->MySQL</li>
                                <li class="mb-2"><svg class="svg-inline--fa fa-check fa-w-16 mr-2 text-primary" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"></path></svg><!-- <i class="fas fa-check mr-2 text-primary"></i> -->MongoDB</li>
                            </ul>
                        </div><!--//skills-block-inner-->
                    </div><!--//skills-block-->

                    <div class="skills-block col-12 col-lg-4">
                        <div class="skills-block-inner bg-white shadow-sm py-4 px-5 position-relative">
                            <h4 class="skills-cat text-center mb-3 mt-5">Others</h4>
                            <div class="skills-icon-holder position-absolute d-inline-block rounded-circle text-center">
                                <img class="skills-icon" src="{{ asset('blog/index/other-skills-icon.svg') }}">
                            </div>
                            <ul class="skills-list list-unstyled text-secondary">
                                <li class="mb-2"><svg class="svg-inline--fa fa-check fa-w-16 mr-2 text-primary" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"></path></svg><!-- <i class="fas fa-check mr-2 text-primary"></i> -->Photoshops</li>
                                <li class="mb-2"><svg class="svg-inline--fa fa-check fa-w-16 mr-2 text-primary" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"></path></svg><!-- <i class="fas fa-check mr-2 text-primary"></i> -->Unit Testing</li>
                                <li class="mb-2"><svg class="svg-inline--fa fa-check fa-w-16 mr-2 text-primary" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"></path></svg><!-- <i class="fas fa-check mr-2 text-primary"></i> -->UX/Wireframing</li>
                                <li class="mb-2"><svg class="svg-inline--fa fa-check fa-w-16 mr-2 text-primary" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"></path></svg><!-- <i class="fas fa-check mr-2 text-primary"></i> -->Balsamiq</li>
                                <li class="mb-2"><svg class="svg-inline--fa fa-check fa-w-16 mr-2 text-primary" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="check" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M173.898 439.404l-166.4-166.4c-9.997-9.997-9.997-26.206 0-36.204l36.203-36.204c9.997-9.998 26.207-9.998 36.204 0L192 312.69 432.095 72.596c9.997-9.997 26.207-9.997 36.204 0l36.203 36.204c9.997 9.997 9.997 26.206 0 36.204l-294.4 294.401c-9.998 9.997-26.207 9.997-36.204-.001z"></path></svg><!-- <i class="fas fa-check mr-2 text-primary"></i> -->Wordpress</li>
                            </ul>
                        </div><!--//skills-block-inner-->
                    </div><!--//skills-block-->

                </div><!--//row-->
            </div>
        </div><!--//container-->
    </section><!--//skills-section-->

    <section class="section-proof section bg-white py-5">
{{--        <div class="container">--}}
{{--            <h3 class="section-title font-weight-bold text-center mb-5">Great clients Ive worked with</h3>--}}
{{--            <div class="logos row mb-5">--}}
{{--                <div class="logo px-3 col-6 col-md-4 col-lg-2 mr-0 px-md-5 px-lg-4"><a class="logo-link" href="https://themes.3rdwavemedia.com/instance/bs4/project.html"><img class="img-fluid svg-ie-fix" src="{{ asset('blog/index/logo-1.svg') }}"></a></div>--}}
{{--                <div class="logo px-3 col-6 col-md-4 col-lg-2 mr-0 px-md-5 px-lg-4"><a class="logo-link" href="https://themes.3rdwavemedia.com/instance/bs4/project.html"><img class="img-fluid svg-ie-fix" src="{{ asset('blog/index/logo-2.svg') }}"></a></div>--}}
{{--                <div class="logo px-3 col-6 col-md-4 col-lg-2 mr-0 px-md-5 px-lg-4"><a class="logo-link" href="https://themes.3rdwavemedia.com/instance/bs4/project.html"><img class="img-fluid svg-ie-fix" src="{{ asset('blog/index/logo-3.svg') }}"></a></div>--}}
{{--                <div class="logo px-3 col-6 col-md-4 col-lg-2 mr-0 px-md-5 px-lg-4"><a class="logo-link" href="https://themes.3rdwavemedia.com/instance/bs4/project.html"><img class="img-fluid svg-ie-fix" src="{{ asset('blog/index/logo-4.svg') }}"></a></div>--}}
{{--                <div class="logo px-3 col-6 col-md-4 col-lg-2 mr-0 px-md-5 px-lg-4"><a class="logo-link" href="https://themes.3rdwavemedia.com/instance/bs4/project.html"><img class="img-fluid svg-ie-fix" src="{{ asset('blog/index/logo-5.svg') }}"></a></div>--}}
{{--                <div class="logo px-3 col-6 col-md-4 col-lg-2 mr-0 px-md-5 px-lg-4"><a class="logo-link" href="https://themes.3rdwavemedia.com/instance/bs4/project.html"><img class="img-fluid svg-ie-fix" src="{{ asset('blog/index/logo-6.svg') }}"></a></div>--}}
{{--            </div>--}}
{{--        </div>--}}

        <div class="container-fluid">

            <div class="testimonials flickity-enabled is-draggable" tabindex="0">

                <!--//testimonial-item-->

                <!--//testimonial-item-->

                <!--//testimonial-item-->

                <!--//testimonial-item-->
                <div class="flickity-viewport" style="touch-action: pan-y;"><div class="flickity-slider" style="left: 0px; transform: translateX(20%);"><div class="testimonial-item is-selected" aria-selected="true" style="position: absolute; left: 0%;">
                            <div class="position-relative p-5 shadow-lg">
                                <blockquote class="blockquote pl-4">
                                    <p class="mb-4">Steve is a great developer! Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto.</p>
                                    <svg class="svg-inline--fa fa-quote-left fa-w-16 quote-icon fa-lg position-absolute text-primary" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="quote-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M464 256h-80v-64c0-35.3 28.7-64 64-64h8c13.3 0 24-10.7 24-24V56c0-13.3-10.7-24-24-24h-8c-88.4 0-160 71.6-160 160v240c0 26.5 21.5 48 48 48h128c26.5 0 48-21.5 48-48V304c0-26.5-21.5-48-48-48zm-288 0H96v-64c0-35.3 28.7-64 64-64h8c13.3 0 24-10.7 24-24V56c0-13.3-10.7-24-24-24h-8C71.6 32 0 103.6 0 192v240c0 26.5 21.5 48 48 48h128c26.5 0 48-21.5 48-48V304c0-26.5-21.5-48-48-48z"></path></svg><!-- <i class="quote-icon fas fa-quote-left fa-lg position-absolute text-primary"></i> -->
                                    <footer class="blockquote-footer presudo-hidden">
                                        <div class="media client-profile flex-column flex-md-row">
                                            <img class="mr-md-3" src="{{ asset('blog/index/profile.jpg') }}" alt="">
                                            <cite title="Source" class="quote-source d-inline-block font-style-normal pt-3">
                                                <span class="d-block">Adam Wells</span>
                                                <span class="d-block">Product Manager, Google</span>
                                                <span class="d-block position-absolute source-link"><svg class="svg-inline--fa fa-linkedin fa-w-14 fa-2x mr-1" data-fa-transform="down-3" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="linkedin" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="" style="transform-origin: 0.4375em 0.6875em;"><g transform="translate(224 256)"><g transform="translate(0, 96)  scale(1, 1)  rotate(0 0 0)"><path fill="currentColor" d="M416 32H31.9C14.3 32 0 46.5 0 64.3v383.4C0 465.5 14.3 480 31.9 480H416c17.6 0 32-14.5 32-32.3V64.3c0-17.8-14.4-32.3-32-32.3zM135.4 416H69V202.2h66.5V416zm-33.2-243c-21.3 0-38.5-17.3-38.5-38.5S80.9 96 102.2 96c21.2 0 38.5 17.3 38.5 38.5 0 21.3-17.2 38.5-38.5 38.5zm282.1 243h-66.4V312c0-24.8-.5-56.7-34.5-56.7-34.6 0-39.9 27-39.9 54.9V416h-66.4V202.2h63.7v29.2h.9c8.9-16.8 30.6-34.5 62.9-34.5 67.2 0 79.7 44.3 79.7 101.9V416z" transform="translate(-224 -256)"></path></g></g></svg><!-- <i class="fab fa-linkedin fa-2x mr-1" data-fa-transform="down-3"></i> --> <a class="text-secondary" href="https://themes.3rdwavemedia.com/instance/bs4/#" target="_blank">View on Linkedin <svg class="svg-inline--fa fa-external-link-alt fa-w-18 ml-1" data-fa-transform="up-1" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="external-link-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg="" style="transform-origin: 0.5625em 0.4375em;"><g transform="translate(288 256)"><g transform="translate(0, -32)  scale(1, 1)  rotate(0 0 0)"><path fill="currentColor" d="M576 24v127.984c0 21.461-25.96 31.98-40.971 16.971l-35.707-35.709-243.523 243.523c-9.373 9.373-24.568 9.373-33.941 0l-22.627-22.627c-9.373-9.373-9.373-24.569 0-33.941L442.756 76.676l-35.703-35.705C391.982 25.9 402.656 0 424.024 0H552c13.255 0 24 10.745 24 24zM407.029 270.794l-16 16A23.999 23.999 0 0 0 384 303.765V448H64V128h264a24.003 24.003 0 0 0 16.97-7.029l16-16C376.089 89.851 365.381 64 344 64H48C21.49 64 0 85.49 0 112v352c0 26.51 21.49 48 48 48h352c26.51 0 48-21.49 48-48V287.764c0-21.382-25.852-32.09-40.971-16.97z" transform="translate(-288 -256)"></path></g></g></svg><!-- <i class="fas fa-external-link-alt ml-1" data-fa-transform="up-1"></i> --></a></span>
                                            </cite>
                                        </div><!--//media-->
                                    </footer>
                                </blockquote>
                            </div>
                        </div><div class="testimonial-item" aria-selected="false" style="position: absolute; left: 63.64%;">
                            <div class="position-relative p-5 shadow-lg">
                                <blockquote class="blockquote pl-4">
                                    <p class="mb-4">Steve is a great developer! Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto.</p>
                                    <svg class="svg-inline--fa fa-quote-left fa-w-16 quote-icon position-absolute text-primary" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="quote-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M464 256h-80v-64c0-35.3 28.7-64 64-64h8c13.3 0 24-10.7 24-24V56c0-13.3-10.7-24-24-24h-8c-88.4 0-160 71.6-160 160v240c0 26.5 21.5 48 48 48h128c26.5 0 48-21.5 48-48V304c0-26.5-21.5-48-48-48zm-288 0H96v-64c0-35.3 28.7-64 64-64h8c13.3 0 24-10.7 24-24V56c0-13.3-10.7-24-24-24h-8C71.6 32 0 103.6 0 192v240c0 26.5 21.5 48 48 48h128c26.5 0 48-21.5 48-48V304c0-26.5-21.5-48-48-48z"></path></svg><!-- <i class="quote-icon fas fa-quote-left position-absolute text-primary"></i> -->
                                    <footer class="blockquote-footer presudo-hidden">
                                        <div class="media client-profile">
                                            <img class="mr-3" src="{{ asset('blog/index/profile.jpg') }}" alt="">
                                            <cite title="Source" class="quote-source d-inline-block font-style-normal pt-3">
                                                <span class="d-block">Cheryl Bell</span>
                                                <span class="d-block">Product Manager, Dropbox</span>
                                                <span class="d-block position-absolute source-link"><svg class="svg-inline--fa fa-linkedin fa-w-14 fa-2x mr-1" data-fa-transform="down-3" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="linkedin" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="" style="transform-origin: 0.4375em 0.6875em;"><g transform="translate(224 256)"><g transform="translate(0, 96)  scale(1, 1)  rotate(0 0 0)"><path fill="currentColor" d="M416 32H31.9C14.3 32 0 46.5 0 64.3v383.4C0 465.5 14.3 480 31.9 480H416c17.6 0 32-14.5 32-32.3V64.3c0-17.8-14.4-32.3-32-32.3zM135.4 416H69V202.2h66.5V416zm-33.2-243c-21.3 0-38.5-17.3-38.5-38.5S80.9 96 102.2 96c21.2 0 38.5 17.3 38.5 38.5 0 21.3-17.2 38.5-38.5 38.5zm282.1 243h-66.4V312c0-24.8-.5-56.7-34.5-56.7-34.6 0-39.9 27-39.9 54.9V416h-66.4V202.2h63.7v29.2h.9c8.9-16.8 30.6-34.5 62.9-34.5 67.2 0 79.7 44.3 79.7 101.9V416z" transform="translate(-224 -256)"></path></g></g></svg><!-- <i class="fab fa-linkedin fa-2x mr-1" data-fa-transform="down-3"></i> --> <a class="text-secondary" href="https://themes.3rdwavemedia.com/instance/bs4/#" target="_blank">View on Linkedin <svg class="svg-inline--fa fa-external-link-alt fa-w-18 ml-1" data-fa-transform="up-1" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="external-link-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg="" style="transform-origin: 0.5625em 0.4375em;"><g transform="translate(288 256)"><g transform="translate(0, -32)  scale(1, 1)  rotate(0 0 0)"><path fill="currentColor" d="M576 24v127.984c0 21.461-25.96 31.98-40.971 16.971l-35.707-35.709-243.523 243.523c-9.373 9.373-24.568 9.373-33.941 0l-22.627-22.627c-9.373-9.373-9.373-24.569 0-33.941L442.756 76.676l-35.703-35.705C391.982 25.9 402.656 0 424.024 0H552c13.255 0 24 10.745 24 24zM407.029 270.794l-16 16A23.999 23.999 0 0 0 384 303.765V448H64V128h264a24.003 24.003 0 0 0 16.97-7.029l16-16C376.089 89.851 365.381 64 344 64H48C21.49 64 0 85.49 0 112v352c0 26.51 21.49 48 48 48h352c26.51 0 48-21.49 48-48V287.764c0-21.382-25.852-32.09-40.971-16.97z" transform="translate(-288 -256)"></path></g></g></svg><!-- <i class="fas fa-external-link-alt ml-1" data-fa-transform="up-1"></i> --></a></span>
                                            </cite>
                                        </div><!--//media-->
                                    </footer>
                                </blockquote>
                            </div>
                        </div><div class="testimonial-item" aria-selected="false" style="position: absolute; left: 127.28%;">
                            <div class="position-relative p-5 shadow-lg">
                                <blockquote class="blockquote pl-4">
                                    <p class="mb-4">Steve is a great developer! Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto. </p>
                                    <svg class="svg-inline--fa fa-quote-left fa-w-16 quote-icon position-absolute text-primary" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="quote-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M464 256h-80v-64c0-35.3 28.7-64 64-64h8c13.3 0 24-10.7 24-24V56c0-13.3-10.7-24-24-24h-8c-88.4 0-160 71.6-160 160v240c0 26.5 21.5 48 48 48h128c26.5 0 48-21.5 48-48V304c0-26.5-21.5-48-48-48zm-288 0H96v-64c0-35.3 28.7-64 64-64h8c13.3 0 24-10.7 24-24V56c0-13.3-10.7-24-24-24h-8C71.6 32 0 103.6 0 192v240c0 26.5 21.5 48 48 48h128c26.5 0 48-21.5 48-48V304c0-26.5-21.5-48-48-48z"></path></svg><!-- <i class="quote-icon fas fa-quote-left position-absolute text-primary"></i> -->
                                    <footer class="blockquote-footer presudo-hidden">
                                        <div class="media client-profile">
                                            <img class="mr-3" src="{{ asset('blog/index/profile.jpg') }}" alt="">
                                            <cite title="Source" class="quote-source d-inline-block font-style-normal pt-3">
                                                <span class="d-block">David Lucas</span>
                                                <span class="d-block">Product Manager, Dropbox</span>
                                                <span class="d-block position-absolute source-link"><svg class="svg-inline--fa fa-linkedin fa-w-14 fa-2x mr-1" data-fa-transform="down-3" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="linkedin" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="" style="transform-origin: 0.4375em 0.6875em;"><g transform="translate(224 256)"><g transform="translate(0, 96)  scale(1, 1)  rotate(0 0 0)"><path fill="currentColor" d="M416 32H31.9C14.3 32 0 46.5 0 64.3v383.4C0 465.5 14.3 480 31.9 480H416c17.6 0 32-14.5 32-32.3V64.3c0-17.8-14.4-32.3-32-32.3zM135.4 416H69V202.2h66.5V416zm-33.2-243c-21.3 0-38.5-17.3-38.5-38.5S80.9 96 102.2 96c21.2 0 38.5 17.3 38.5 38.5 0 21.3-17.2 38.5-38.5 38.5zm282.1 243h-66.4V312c0-24.8-.5-56.7-34.5-56.7-34.6 0-39.9 27-39.9 54.9V416h-66.4V202.2h63.7v29.2h.9c8.9-16.8 30.6-34.5 62.9-34.5 67.2 0 79.7 44.3 79.7 101.9V416z" transform="translate(-224 -256)"></path></g></g></svg><!-- <i class="fab fa-linkedin fa-2x mr-1" data-fa-transform="down-3"></i> --> <a class="text-secondary" href="https://themes.3rdwavemedia.com/instance/bs4/#" target="_blank">View on Linkedin <svg class="svg-inline--fa fa-external-link-alt fa-w-18 ml-1" data-fa-transform="up-1" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="external-link-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg="" style="transform-origin: 0.5625em 0.4375em;"><g transform="translate(288 256)"><g transform="translate(0, -32)  scale(1, 1)  rotate(0 0 0)"><path fill="currentColor" d="M576 24v127.984c0 21.461-25.96 31.98-40.971 16.971l-35.707-35.709-243.523 243.523c-9.373 9.373-24.568 9.373-33.941 0l-22.627-22.627c-9.373-9.373-9.373-24.569 0-33.941L442.756 76.676l-35.703-35.705C391.982 25.9 402.656 0 424.024 0H552c13.255 0 24 10.745 24 24zM407.029 270.794l-16 16A23.999 23.999 0 0 0 384 303.765V448H64V128h264a24.003 24.003 0 0 0 16.97-7.029l16-16C376.089 89.851 365.381 64 344 64H48C21.49 64 0 85.49 0 112v352c0 26.51 21.49 48 48 48h352c26.51 0 48-21.49 48-48V287.764c0-21.382-25.852-32.09-40.971-16.97z" transform="translate(-288 -256)"></path></g></g></svg><!-- <i class="fas fa-external-link-alt ml-1" data-fa-transform="up-1"></i> --></a></span>
                                            </cite>
                                        </div><!--//media-->
                                    </footer>
                                </blockquote>
                            </div>
                        </div><div class="testimonial-item" aria-selected="false" style="position: absolute; left: -63.64%;">
                            <div class="position-relative p-5 shadow-lg">
                                <blockquote class="blockquote pl-4">
                                    <p class="mb-4">Steve is a great developer! Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto.</p>
                                    <svg class="svg-inline--fa fa-quote-left fa-w-16 quote-icon position-absolute text-primary" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="quote-left" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M464 256h-80v-64c0-35.3 28.7-64 64-64h8c13.3 0 24-10.7 24-24V56c0-13.3-10.7-24-24-24h-8c-88.4 0-160 71.6-160 160v240c0 26.5 21.5 48 48 48h128c26.5 0 48-21.5 48-48V304c0-26.5-21.5-48-48-48zm-288 0H96v-64c0-35.3 28.7-64 64-64h8c13.3 0 24-10.7 24-24V56c0-13.3-10.7-24-24-24h-8C71.6 32 0 103.6 0 192v240c0 26.5 21.5 48 48 48h128c26.5 0 48-21.5 48-48V304c0-26.5-21.5-48-48-48z"></path></svg><!-- <i class="quote-icon fas fa-quote-left position-absolute text-primary"></i> -->
                                    <footer class="blockquote-footer presudo-hidden">
                                        <div class="media client-profile">
                                            <img class="mr-3" src="{{ asset('blog/index/profile.jpg') }}" alt="">
                                            <cite title="Source" class="quote-source d-inline-block font-style-normal pt-3">
                                                <span class="d-block">Brandon Campbell</span>
                                                <span class="d-block">Product Manager, Airbnb</span>
                                                <span class="d-block position-absolute source-link"><svg class="svg-inline--fa fa-linkedin fa-w-14 fa-2x mr-1" data-fa-transform="down-3" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="linkedin" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg="" style="transform-origin: 0.4375em 0.6875em;"><g transform="translate(224 256)"><g transform="translate(0, 96)  scale(1, 1)  rotate(0 0 0)"><path fill="currentColor" d="M416 32H31.9C14.3 32 0 46.5 0 64.3v383.4C0 465.5 14.3 480 31.9 480H416c17.6 0 32-14.5 32-32.3V64.3c0-17.8-14.4-32.3-32-32.3zM135.4 416H69V202.2h66.5V416zm-33.2-243c-21.3 0-38.5-17.3-38.5-38.5S80.9 96 102.2 96c21.2 0 38.5 17.3 38.5 38.5 0 21.3-17.2 38.5-38.5 38.5zm282.1 243h-66.4V312c0-24.8-.5-56.7-34.5-56.7-34.6 0-39.9 27-39.9 54.9V416h-66.4V202.2h63.7v29.2h.9c8.9-16.8 30.6-34.5 62.9-34.5 67.2 0 79.7 44.3 79.7 101.9V416z" transform="translate(-224 -256)"></path></g></g></svg><!-- <i class="fab fa-linkedin fa-2x mr-1" data-fa-transform="down-3"></i> --> <a class="text-secondary" href="https://themes.3rdwavemedia.com/instance/bs4/#" target="_blank">View on Linkedin <svg class="svg-inline--fa fa-external-link-alt fa-w-18 ml-1" data-fa-transform="up-1" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="external-link-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg="" style="transform-origin: 0.5625em 0.4375em;"><g transform="translate(288 256)"><g transform="translate(0, -32)  scale(1, 1)  rotate(0 0 0)"><path fill="currentColor" d="M576 24v127.984c0 21.461-25.96 31.98-40.971 16.971l-35.707-35.709-243.523 243.523c-9.373 9.373-24.568 9.373-33.941 0l-22.627-22.627c-9.373-9.373-9.373-24.569 0-33.941L442.756 76.676l-35.703-35.705C391.982 25.9 402.656 0 424.024 0H552c13.255 0 24 10.745 24 24zM407.029 270.794l-16 16A23.999 23.999 0 0 0 384 303.765V448H64V128h264a24.003 24.003 0 0 0 16.97-7.029l16-16C376.089 89.851 365.381 64 344 64H48C21.49 64 0 85.49 0 112v352c0 26.51 21.49 48 48 48h352c26.51 0 48-21.49 48-48V287.764c0-21.382-25.852-32.09-40.971-16.97z" transform="translate(-288 -256)"></path></g></g></svg><!-- <i class="fas fa-external-link-alt ml-1" data-fa-transform="up-1"></i> --></a></span>
                                            </cite>
                                        </div><!--//media-->
                                    </footer>
                                </blockquote>
                            </div>
                        </div></div></div><button class="flickity-button flickity-prev-next-button previous" type="button" aria-label="Previous"><svg class="flickity-button-icon" viewBox="0 0 100 100"><path d="M 10,50 L 60,100 L 65,95 L 20,50  L 65,5 L 60,0 Z" class="arrow"></path></svg></button><button class="flickity-button flickity-prev-next-button next" type="button" aria-label="Next"><svg class="flickity-button-icon" viewBox="0 0 100 100"><path d="M 10,50 L 60,100 L 65,95 L 20,50  L 65,5 L 60,0 Z" class="arrow" transform="translate(100, 100) rotate(180) "></path></svg></button><ol class="flickity-page-dots"><li class="dot" aria-label="Page dot 1" aria-current="step"></li><li class="dot" aria-label="Page dot 2"></li><li class="dot" aria-label="Page dot 3"></li><li class="dot" aria-label="Page dot 4"></li></ol></div><!--//container-fluid-->
        </div><!--//testimonials-->
    </section>

    <section class="section-featured-projects py-5">
        <div class="container">
            <h3 class="section-title font-weight-bold text-center mb-5">{{ trans('home.featured_projects') }}</h3>

            <div class="project-cards row mb-5">
                @if(count($projects) > 0)
                    @foreach($projects as $project)
                        <div class="col-12 col-lg-4">
                            <div class="card rounded-0 border-0 shadow-sm mb-5 mb-lg-0">
                                <div class="card-img-container position-relative">
                                    <img style="height: 210px" class="card-img-top rounded-0" src="{{ asset("storage/$project->image") }}" alt="">
                                    <a class="card-img-overlay overlay-content text-left p-lg-4" href="{{ route('post.show', [$category_project->slug, $project->slug]) }}">
                                        <h5 class="card-title font-weight-bold">{{ $project->title }}</h5>
                                        <p class="card-text">{{ $project->description }}</p>
                                    </a>
                                </div>
                                <div class="card-body pb-0">
                                    <h4 class="card-title text-truncate text-center mb-0"><a href="{{ route('post.show', [$category_project->slug, $project->slug]) }}">{{ $project->title }}</a></h4>
                                </div>

                                <div class="card-footer border-0 text-center bg-white pb-4">
                                    <ul class="list-inline mb-0 mx-auto">
                                        @if(count($project->skills) > 0)
                                            @foreach($project->skills as $skill)
                                                <li class="list-inline-item"><span class="badge badge-secondary badge-pill">{{ $skill }}</span></li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                            </div><!--//card-->
                        </div>
                    @endforeach
                @endif
            </div><!--//row-->
            <div class="text-center">
                <a class="btn btn-contact" style="border-radius: 50px;" href="{{ route('post', $category_project->slug) }}">{{ trans('home.all_projects') }}</a>
            </div>
        </div><!--///container-->

    </section>

    <section class="section-latest-blog py-5">
        <div class="container">
            <h3 class="section-title font-weight-bold text-center mb-5">{{ trans('home.featured_post_blog') }}</h3>

            <div class="blog-cards row">
                @if(count($posts) > 0)
                    @foreach($posts as $post)
                    <div class="col-12 col-lg-6">
                        <div class="card rounded-0 border-0 shadow-sm mb-5">
                            <div class="card-img-container position-relative">
                                <img class="card-img-top rounded-0" style="height: 285px;" src="{{ asset("storage/$post->image") }}" alt="">
                                <div class="card-img-overlay overlay-mask overlay-logo text-center p-0">
                                    <div class="overlay-mask-content text-center w-100 position-absolute">
                                        {{--<a class="btn btn-primary" href="{{ route('post.show', [$category_post->slug, $post->slug]) }}">Read more</a>--}}
                                    </div>
                                    <a class="overlay-mask-link position-absolute w-100 h-100" href="{{ route('post.show', [$category_post->slug, $post->slug]) }}"></a>
                                </div>
                            </div>
                            <div class="card-body pb-4">
                                <h4 class="card-title text-truncate mb-2"><a href="{{ route('post.show', [$category_post->slug, $post->slug]) }}">{{ $post->title }}</a></h4>
                                <div class="card-text">
                                    <ul class="meta list-inline mb-1">
                                        <li class="list-inline-item mr-3"><svg class="svg-inline--fa fa-clock fa-w-16 mr-2" aria-hidden="true" focusable="false" data-prefix="far" data-icon="clock" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm61.8-104.4l-84.9-61.7c-3.1-2.3-4.9-5.9-4.9-9.7V116c0-6.6 5.4-12 12-12h32c6.6 0 12 5.4 12 12v141.7l66.8 48.6c5.4 3.9 6.5 11.4 2.6 16.8L334.6 349c-3.9 5.3-11.4 6.5-16.8 2.6z"></path></svg><!-- <i class="far fa-clock mr-2"></i> -->{{ $post->created_at }}</li>
                                    </ul>
                                </div>
                            </div>
                        </div><!--//card-->
                    </div>
                    @endforeach
                @endif
        </div>
        <div class="text-center">
            <a class="btn btn-contact"  style="border-radius: 50px;" href="{{ route('post', $category_post->slug) }}">{{ trans('home.all_posts_blog') }}</a>
        </div>
    </section>
    <section class="section-featured-projects py-5">
        <div class="container">
            <h3 class="section-title font-weight-bold text-center mb-5">{{ trans('home.image_me') }}</h3>
            <div class="project-cards row mb-5">
                <div class="col-12 col-lg-4" style="height: 250px; overflow: hidden;">
                    <div class="card rounded-0 border-0 shadow-sm mb-5 mb-lg-0">
                        <div class="card-img-container position-relative">
                            <img style="height: 100%; background-size: cover; " class="card-img-top rounded-0" src="{{ asset("blog/index/image-profile-3.jpg") }}" alt="">
                        </div>
                    </div>
                </div>        

                <div class="col-12 col-lg-4" style="height: 250px; overflow: hidden;">
                    <div class="card rounded-0 border-0 shadow-sm mb-5 mb-lg-0">
                        <div class="card-img-container position-relative">
                            <img style="height: 100%; background-size: cover; " class="card-img-top rounded-0" src="{{ asset("blog/index/image-profile-2.jpg") }}" alt="">
                        </div>
                    </div>
                </div>    

              <div class="col-12 col-lg-4" style="height: 250px; overflow: hidden;">
                    <div class="card rounded-0 border-0 shadow-sm mb-5 mb-lg-0">
                        <div class="card-img-container position-relative">
                            <img style="height: 100%; background-size: cover; " class="card-img-top rounded-0" src="{{ asset("blog/index/image-profile-1.jpg") }}" alt="">
                        </div>
                    </div>
                </div>   

            </div><!--//row-->
            <div class="text-center">
                <a class="btn btn-contact" style="border-radius: 50px;" href="https://www.facebook.com/zenzen1996/photos_all?lst=100009309186643%3A100009309186643%3A1561360702">{{ trans('home.all_image_of_me_on_fb') }}</a>
            </div>
        </div><!--///container-->

    </section>
@endsection
