@extends('blog.layout')
@section('title')
    {{ $category->title }}
@endsection
@section('css')
    style="min-height: 50vh !important; 
    background-size: cover; 
    background-image: url('{{ asset("storage/$category->header_background")}}') !important;"
@endsection
@section('header')
<div style="opacity: 1;transform: translateY(0px) translateZ(0px); margin: 50px 0px;">
    <div class="header-intro text-white py-5">
        <div class="container text-center">
            <h2 class="page-heading mb-2">{{ $category->title }}</h2>
            <div class="page-heading mb-2" style="font-size: 20px">{{ $category->header_description }}</div>
        </div><!--//container-->
    </div>
</div>
@endsection
@section('content')
    <section class="section pt-5">
        <div class="container">
            @if(count($posts) > 0)
                @foreach($posts as $post)
                    <div class="media mb-5 flex-column flex-lg-row bg-white shadow-sm">
                        <div class="talk-media-holder embed-responsive embed-responsive-16by9 mr-md-3">
                            <iframe class="embed-responsive-item" width="560" height="315" src="{{ $post->link_youtube }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>
                        </div>

                        <div class="media-body p-4">
                            <h4> <a href="{{ route('post.show',[$category->slug, $post->slug]) }}">{{ $post->title }}</a></h4>
                            <div class="talk-content">
                                <p>{{ $post->description }}</p>
                            </div>
                            <ul class="talk-meta list-inline mb-2">
                                <li class="list-inline-item mr-3"><svg class="svg-inline--fa fa-clock fa-w-16 mr-2" aria-hidden="true" focusable="false" data-prefix="far" data-icon="clock" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm61.8-104.4l-84.9-61.7c-3.1-2.3-4.9-5.9-4.9-9.7V116c0-6.6 5.4-12 12-12h32c6.6 0 12 5.4 12 12v141.7l66.8 48.6c5.4 3.9 6.5 11.4 2.6 16.8L334.6 349c-3.9 5.3-11.4 6.5-16.8 2.6z"></path></svg><!-- <i class="far fa-clock mr-2"></i> -->{{ $post->created_at }}</li>
                                <li class="list-inline-item"><svg class="svg-inline--fa fa-video fa-w-18 mr-2" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="video" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" data-fa-i2svg=""><path fill="currentColor" d="M336.2 64H47.8C21.4 64 0 85.4 0 111.8v288.4C0 426.6 21.4 448 47.8 448h288.4c26.4 0 47.8-21.4 47.8-47.8V111.8c0-26.4-21.4-47.8-47.8-47.8zm189.4 37.7L416 177.3v157.4l109.6 75.5c21.2 14.6 50.4-.3 50.4-25.8V127.5c0-25.4-29.1-40.4-50.4-25.8z"></path></svg><!-- <i class="fas fa-video mr-2"></i> -->Video</li>
                            </ul>
                        </div>
                    </div><!--//media-->
                @endforeach
            @endif
        </div><!--//container-->
    </section>
@endsection
