@extends('blog.layout')
@section('title')
    {{ $category->title }}
@endsection
@section('css')
    style="min-height: 50vh !important; 
    background-size: cover; 
    background-image: url('{{ asset("storage/$category->header_background")}}') !important;"
@endsection
@section('header')
<div style="opacity: 1;transform: translateY(0px) translateZ(0px); margin: 50px 0px;">
    <div class="header-intro text-white py-5">
        <div class="container text-center">
            <h2 class="page-heading mb-2">{{ $category->title }}</h2>
            <div class="page-heading mb-2" style="font-size: 20px">{{ $category->header_description }}</div>
        </div><!--//container-->
    </div>
</div>
@endsection
@section('content')
    <section class="section pt-5">
        <div class="container blog-cards">

            <div class="row">
                @if(count($posts) > 0)
                    @foreach($posts as $post)
                        <div class="col-12 col-md-6 col-lg-4 mb-5">
                            <div class="card rounded-0 border-0 shadow-sm eq-height">
                                <div class="card-img-container position-relative">
                                    <img class="card-img-top rounded-0" style="height: 180px;" src="{{ asset("storage/$post->image") }}" alt="">
                                    <div class="card-img-overlay overlay-mask text-center p-0">
                                        {{--  <div class="overlay-mask-content text-center w-100 position-absolute">
                                            <a class="btn btn-primary" href="{{ route('post.show', [$category->slug, $post->slug]) }}">Đọc th</a>
                                        </div>  --}}
                                        <a class="overlay-mask-link position-absolute w-100 h-100" href="{{ route('post.show', [$category->slug, $post->slug]) }}"></a>
                                    </div>
                                </div>
                                <div class="card-body pb-4">

                                    <h4 class="card-title mb-2"><a href="{{ route('post.show', [$category->slug, $post->slug]) }}">{{ $post->title }}</a></h4>
                                    <div class="card-text">

                                        <div class="excerpt">{{ $post->description }}</div>
                                    </div>

                                </div>
                                <div class="card-footer border-0">
                                    <ul class="meta list-inline mb-0">
                                        <li class="list-inline-item mr-3"><svg class="svg-inline--fa fa-clock fa-w-16 mr-2" aria-hidden="true" focusable="false" data-prefix="far" data-icon="clock" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm61.8-104.4l-84.9-61.7c-3.1-2.3-4.9-5.9-4.9-9.7V116c0-6.6 5.4-12 12-12h32c6.6 0 12 5.4 12 12v141.7l66.8 48.6c5.4 3.9 6.5 11.4 2.6 16.8L334.6 349c-3.9 5.3-11.4 6.5-16.8 2.6z"></path></svg><!-- <i class="far fa-clock mr-2"></i> -->{{ $post->created_at }}</li>
                                        {{--<li class="list-inline-item"><svg class="svg-inline--fa fa-comment fa-w-16 mr-2" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="comment" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M256 32C114.6 32 0 125.1 0 240c0 49.6 21.4 95 57 130.7C44.5 421.1 2.7 466 2.2 466.5c-2.2 2.3-2.8 5.7-1.5 8.7S4.8 480 8 480c66.3 0 116-31.8 140.6-51.4 32.7 12.3 69 19.4 107.4 19.4 141.4 0 256-93.1 256-208S397.4 32 256 32z"></path></svg><!-- <i class="fas fa-comment mr-2"></i> --><a href="#">12 Comments</a></li>--}}
                                    </ul>
                                </div>
                            </div><!--//card-->
                        </div>
                    @endforeach
                @endif
                {{--  <div class="col-12 mb-5">
                    <a class="btn-blog-more btn btn-primary btn-block text-white font-weight-bold">Load more posts...</a>
                </div>  --}}
            </div>
        </div><!--//container-->
    </section>
@endsection
