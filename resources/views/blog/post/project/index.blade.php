@extends('blog.layout')
@section('title')
    {{ $category->title }}
@endsection
@section('css')
    style="min-height: 50vh !important; 
    background-size: cover; 
    background-image: url('{{ asset("storage/$category->header_background")}}') !important;"
@endsection
@section('header')
<div style="opacity: 1;transform: translateY(0px) translateZ(0px); margin: 50px 0px;">
    <div class="header-intro text-white py-5">
        <div class="container text-center">
            <h2 class="page-heading mb-3">{{ $category->title }}</h2>
            <p class="page-heading mb-3" style="font-size: 20px;">{{ $category->header_description }}</p>
        </div><!--//container-->
    </div>
</div>
@endsection
@section('content')
    <section class="section pt-5">
        <div class="container">
            <div class="text-center">
                <ul id="filters" class="filters mb-5 mx-auto pl-0">
                    <li class="type active" data-filter="*">All</li>
                    <li class="type" data-filter=".angular">Angular</li>
                    <li class="type" data-filter=".react">React</li>
                    <li class="type" data-filter=".python-django">Python/Django</li>
                    <li class="type" data-filter=".ruby-rails">Ruby/Rails</li>
                    <li class="type" data-filter=".php">PHP</li>
                    <li class="type" data-filter=".vuejs">Vuejs</li>
                </ul><!--//filters-->
            </div>

            <div class="project-cards row mb-5 isotope" style="position: relative; height: 1146.98px;">
               @if(count($posts) > 0)
                   @foreach($posts as $post)
                        <div class="isotope-item col-12 col-lg-4 @if(count($post->skills) > 0) @foreach($post->skills as $skill) {{ strtolower($skill) }}@endforeach @endif" style="position: absolute; left: 0px; top: 0px;">
                            <div class="card rounded-0 border-0 shadow-sm mb-5">
                                <div class="card-img-container position-relative">
                                    <img class="card-img-top rounded-0" style="height: 210px;" src="{{ asset("storage/$post->image")}}" alt="">
                                    <a class="card-img-overlay overlay-content text-left p-lg-4" href="{{ route('post.show', [$category->slug, $post->slug]) }}">
                                        <h5 class="card-title font-weight-bold">{{ $post->title }}</h5>
                                        <p class="card-text">{{ $post->description }}</p>
                                    </a>
                                </div>
                                <div class="card-body pb-0">
                                    <h4 class="card-title text-truncate text-center mb-0"><a href="{{ route('post.show', [$category->slug, $post->slug]) }}">{{ $post->title }}</a></h4>
                                </div>

                                <div class="card-footer border-0 text-center bg-white pb-4">
                                    <ul class="list-inline mb-0 mx-auto">
                                        @if(count($post->skills) > 0)
                                            @foreach($post->skills as $skill)
                                                    <li class="list-inline-item"><span class="badge badge-secondary badge-pill">{{ $skill }}</span></li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                            </div><!--//card-->
                        </div><!--//isotope-item-->
                   @endforeach
               @endif()
            </div><!--//row-->
        </div><!--//container-->
    </section>
@endsection
