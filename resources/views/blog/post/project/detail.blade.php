@extends('blog.layout')
@section('title')
    {{ $post->title }}
@endsection
@section('css')
    style="min-height: 50vh !important; 
    background-size: cover; 
    background-image: url('{{ asset("storage/$post->header_background")}}') !important;"
@endsection
@section('header')
<div style="opacity: 1;transform: translateY(0px) translateZ(0px); margin: 50px 0px;">
    <div class="header-intro text-white py-5">
        <div class="container position-relative text-center">
            <h2 class="page-heading mb-2">{{ $post->title }}</h2>
            <div class="page-heading mb-2" style="font-size: 20px">{{ $post->header_description }}</div>
            {{--<div class="page-heading-logo font-weight-bold position-absolute mt-4 mt-md-0"><img class="client-logo" src=""></div>--}}
        </div><!--//container-->
    </div>
</div>
@endsection
@section('content')
    {!! $post->body !!}
    <section class="section pt-5 related-projects-section bg-white pb-4">
        <div class="container">
            <h3 class="section-title font-weight-bold text-center mb-4">{{ trans('home.project_other') }}</h3>
            <div class="project-cards row">
            @if(count($posts_project_related) > 0)
                @foreach($posts_project_related as $item)
                        <div class="col-12 col-lg-4">
                            <div class="card rounded-0 border-0 shadow-sm mb-5 mb-lg-0">
                                <div class="card-img-container position-relative">
                                    <img class="card-img-top rounded-0" style="height: 210px;" src="{{ asset("storage/$item->image") }}" alt="">
                                    <a class="card-img-overlay overlay-content text-left p-lg-4" href="{{ route('post.show', [$category->slug, $post->slug]) }}">
                                        <h5 class="card-title font-weight-bold">{{$item->title}}</h5>
                                        <p class="card-text">{{$item->description}}</p>
                                    </a>
                                </div>
                                <div class="card-body pb-0">
                                    <h4 class="card-title text-truncate text-center mb-0"><a href="{{ route('post.show', [$category->slug, $post->slug]) }}">{{$item->title}}</a></h4>
                                </div>

                                <div class="card-footer border-0 text-center bg-white pb-4">
                                    <ul class="list-inline mb-0 mx-auto">
                                        @if(count($item->skills) > 0)
                                            @foreach($item->skills as $skill)
                                                <li class="list-inline-item"><span class="badge badge-secondary badge-pill">{{ $skill }}</span></li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>
                            </div><!--//card-->
                        </div>
                @endforeach
            @endif
            </div><!--//row-->
        </div><!--//container-->
    </section>
@endsection()
